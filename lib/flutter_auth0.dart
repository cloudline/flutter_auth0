import 'package:flutter/services.dart';

class FlutterAuth0 {
  final MethodChannel _channel = const MethodChannel('flutter_auth0');

  Future<String> login() async {
    final token = await _channel.invokeMethod('login');
    return token;
  }

  Future<String> getToken() async {
    final String token = await _channel.invokeMethod('getToken');
    return token;
  }

  Future<bool> logout() async {
    return await _channel.invokeMethod('logout');
  }
}
