import 'package:flutter/services.dart';
import 'package:flutter_auth0/flutter_auth0.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const MethodChannel channel = MethodChannel('flutter_auth0');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    FlutterAuth0 auth0 = FlutterAuth0(scheme: 'com.yowlcat', domain: 'dev-yowlcat-auth0');
    expect(await auth0.login(), '42');
  });
}
