package com.cloudlinesolutions.flutter_auth0;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.widget.Toast;

import com.auth0.android.Auth0;
import com.auth0.android.authentication.AuthenticationAPIClient;
import com.auth0.android.authentication.AuthenticationException;
import com.auth0.android.authentication.storage.CredentialsManagerException;
import com.auth0.android.authentication.storage.SecureCredentialsManager;
import com.auth0.android.authentication.storage.SharedPreferencesStorage;
import com.auth0.android.callback.BaseCallback;
import com.auth0.android.provider.AuthCallback;
import com.auth0.android.provider.WebAuthProvider;
import com.auth0.android.result.Credentials;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * FlutterAuth0Plugin
 */
public class FlutterAuth0Plugin implements MethodCallHandler {
    private Activity context;
    private Auth0 auth0;
    private SecureCredentialsManager credentialsManager;

    FlutterAuth0Plugin(Activity activity) {
        this.context = activity;

        auth0 = new Auth0(activity);
        auth0.setOIDCConformant(true);
        credentialsManager = new SecureCredentialsManager(activity, new AuthenticationAPIClient(auth0), new SharedPreferencesStorage(activity));
    }

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_auth0");
        channel.setMethodCallHandler(new FlutterAuth0Plugin(registrar.activity()));
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        switch (call.method) {
            case "login":
                String domain = getResourceFromContext(context, "com_auth0_domain");
                String scheme = getResourceFromContext(context, "com_auth0_scheme");
                doLogin(result, domain, scheme);
                break;
            case "logout":
                credentialsManager.clearCredentials();
                result.success(true);
                break;
            case "getToken":
                getToken(result);
                break;
            default:
                result.notImplemented();
                break;
        }
    }

    private static String getResourceFromContext(Context context, String resName) {
        final int stringRes = context.getResources().getIdentifier(resName, "string", context.getPackageName());
        if (stringRes == 0) {
            throw new IllegalArgumentException(String.format("The 'R.string.%s' value it's not defined in your project's resources file.", resName));
        }
        return context.getString(stringRes);
    }

    private void getToken(final Result result) {
        credentialsManager.getCredentials(new BaseCallback<Credentials, CredentialsManagerException>() {
            @Override
            public void onSuccess(final Credentials credentials) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        credentialsManager.saveCredentials(credentials);
                        result.success(credentials.getAccessToken());
                    }
                });
            }

            @Override
            public void onFailure(CredentialsManagerException error) {
                //Authentication cancelled by the user. Exit the app
                result.error("Get Credentials Error", error.getMessage(), null);
            }
        });
    }

    private void doLogin(final Result result, String domain, String scheme) {
        if (!credentialsManager.hasValidCredentials()) {
            Map<String, Object> params = new HashMap<>();
            params.put("prompt", "login");

            WebAuthProvider.init(auth0)
                .withScheme(scheme)
                .withParameters(params)
                .withScope("offline_access")
                .withAudience(String.format("https://%s/api/v2/", domain))
                .start(context, new AuthCallback() {
                    @Override
                    public void onFailure(final Dialog dialog) {
                        // Show error Dialog to user
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.show();
                            }
                        });
                    }

                    @Override
                    public void onFailure(final AuthenticationException exception) {
                        // Show error to user
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            result.error("Authentication failed", exception.getMessage(), null);
                            Toast.makeText(context, "Error: " + exception.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onSuccess(final Credentials credentials) {
                        // Store credentials
                        // Navigate to your main activity
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            credentialsManager.saveCredentials(credentials);
                            result.success(credentials.getAccessToken());
                            }
                        });
                    }
                });
        } else {
            getToken(result);
        }
    }
}
