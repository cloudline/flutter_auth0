#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = 'flutter_auth0'
  s.version          = '0.0.2'
  s.summary          = 'Flutter Auth0 Plugin'
  s.description      = <<-DESC
Flutter Auth0 Plugin
                       DESC
  s.homepage         = 'http://cloudline-solutions.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Cloudline Solutions' => 'nico@cloudline-solutions.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.dependency 'Auth0', '1.18'
  s.ios.deployment_target = '9.0'
end

