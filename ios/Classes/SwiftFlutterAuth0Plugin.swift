import Flutter
import UIKit
import Auth0

public class SwiftFlutterAuth0Plugin: NSObject, FlutterPlugin {
  let credentialsManager = CredentialsManager(authentication: Auth0.authentication())
    
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_auth0", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterAuth0Plugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }
    
    public func getToken(result: @escaping FlutterResult) {
        credentialsManager.credentials { error, credentials in
            guard error == nil, let credentials = credentials else {
                // Handle error
                print("Error: \(String(describing: error))")
                result("")
                return;
            }
            
            // You now have a valid credentials object, you might want to store this locally for easy access.
            // You will use this later to retrieve the user's profile
            _ = self.credentialsManager.store(credentials: credentials)
            result(credentials.accessToken)
        }
    }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch call.method {
        case "getToken":
            guard credentialsManager.hasValid() else { result(""); return; }
            getToken(result: result)

        case "login":
            guard let clientInfo = plistValues(bundle: Bundle.main) else { return }
            
            guard credentialsManager.hasValid() else {
                Auth0
                    .webAuth()
                    .parameters(["prompt": "login"])
                    .scope("openid profile offline_access")
                    .audience("https://" + clientInfo.domain + "/api/v2/")
                    .start {
                        switch $0 {
                        case .failure(let error):
                            result(FlutterError(code: "UNAUTHORIZED",
                            message: error.localizedDescription,
                            details: "User didn't logged in"))
                        case .success(let credentials):
                            _ = self.credentialsManager.store(credentials: credentials)
                            guard let accessToken = credentials.accessToken else { return }
                            result(accessToken)
                        }
                    }
                return;
            }

            getToken(result: result)
        
        case "logout":
            result(credentialsManager.clear())
        
        default:
            result(FlutterMethodNotImplemented)
    }
  }
}

func plistValues(bundle: Bundle) -> (clientId: String, domain: String)? {
    guard
        let path = bundle.path(forResource: "Auth0", ofType: "plist"),
        let values = NSDictionary(contentsOfFile: path) as? [String: Any]
        else {
            print("Missing Auth0.plist file with 'ClientId' and 'Domain' entries in main bundle!")
            return nil
    }
    
    guard
        let clientId = values["ClientId"] as? String,
        let domain = values["Domain"] as? String
        else {
            print("Auth0.plist file at \(path) is missing 'ClientId' and/or 'Domain' entries!")
            print("File currently has the following entries: \(values)")
            return nil
    }
    return (clientId: clientId, domain: domain)
}
