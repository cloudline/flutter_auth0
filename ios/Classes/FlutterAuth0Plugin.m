#import "FlutterAuth0Plugin.h"
#import <flutter_auth0/flutter_auth0-Swift.h>

@implementation FlutterAuth0Plugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterAuth0Plugin registerWithRegistrar:registrar];
}
@end
