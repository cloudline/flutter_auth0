import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class AppConfig extends InheritedWidget {
  AppConfig({
    @required this.auth0Scheme,
    @required this.auth0Domain,
    @required this.auth0Client,
    @required Widget child,
  }) : super(child: child);

  final String auth0Scheme;
  final String auth0Domain;
  final String auth0Client;

  static AppConfig of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AppConfig);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}