import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_auth0/flutter_auth0.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FlutterAuth0 auth0 = FlutterAuth0();
  String _clientId = '';

  Future<void> _login() async {
    String clientId;
    try {
      clientId = await auth0.login();
      setState(() => _clientId = clientId);
    } on MissingPluginException catch (e) {
      print(e.message);
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> _logout() async {
    try {
      bool loggedOut = await auth0.logout();
      if (loggedOut) {
        setState(() => _clientId = '');
      }
    } on MissingPluginException catch (e) {
      print(e.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Client Id: $_clientId'),
            _clientId == ''
                ? FlatButton(
                    onPressed: () => _login(),
                    child: Text('Login with Auth0'),
                  )
                : FlatButton(
                    onPressed: () => _logout(),
                    child: Text('Log out'),
                  )
          ],
        )),
      ),
    );
  }
}
